'''
* Paths: /home/lessismore/siegeResults/
        /home/lessismore/siegeResults/plots/

* To run: python plot.py *.log

 This script takes log files from the command line,
 and plots the failure rate and std in a bar graph
'''

import sys
import math
import numpy as np
import matplotlib.pyplot as plt

# filter data
def remove(l):
    list = []
    for i in range(len(l)):
        if i % 2 != 0:
            list.append(int(l[i]))
    return list

# get list of files
meanlist = []
stdlist = []
myfiles = []
for arg in sys.argv:
    myfiles.append(arg)
myfiles.pop(0)

# create list of values to plot
for file in myfiles:
    trans = []
    okay = []
    mean = []

    for line in open(file):
        columns = line.split(', ')
        okay.append(columns[8])
        trans.append(columns[1])
    okay = remove(okay)
    trans = remove(trans)

    for i in range(len(list(okay))):
        mean.append(float(trans[i] - okay[i])/trans[i])

    meanlist.append(np.mean(mean))
    stdlist.append(np.std(mean))

# set up plot
N = len(list(myfiles))
width = 0.5
ind = np.arange(N)

p1 = plt.bar(ind, meanlist, width, color='black', yerr=stdlist)

plt.ylabel('Dropped Requests')
plt.xlabel('Pools')
plt.title('Dropped Requests vs. Pools')
plt.xticks(ind+width/2, (list(myfiles)))

'''
# Add legend
str = 'f=first\nh=hdr\nl=leastconn\nrc=rdp-cookie\nrr=roundrobin\ns=source\nu=uri\nup=url-param'
plt.text(0.05, 0.95, str, fontsize=10, verticalalignment='top')
'''

# plot and save
plt.show()
plt.savefig('plot.png')

