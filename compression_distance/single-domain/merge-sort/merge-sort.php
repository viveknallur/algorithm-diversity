

function merge_sort(array $left, array $right) {
    $result = [];

    while (count($left) && count($right)) {
        if ($left[0] < $right[0]) {
            $result[] = array_shift($left);
        } else {
            $result[] = array_shift($right);
        }
    }

    return array_merge($result, $left, $right);
}

function merge(array $arrayToSort) {
    if (count($arrayToSort) == 1) {
        return $arrayToSort;
    }

    $left = merge(array_slice($arrayToSort, 0, count($arrayToSort) / 2));
    $right = merge(array_slice($arrayToSort, count($arrayToSort) / 2, count($arrayToSort)));

    return merge_sort($left, $right);
}

/**
 * Not sorted array, should be split in two chunks and sort them (possible with the same algorithm, but not always)
 */

print_r(merge([7, 1, 8, 9, 10, 4, 2, 5, 6, 0, 3, 25]));

/**
 * Already sorted two chunks
 */

$first = [1, 2, 5, 7, 8 ,10];

$second = [3, 4, 6, 12, 15];

print_r(merge_sort($first, $second));

