func InsertionSort(data []int) {
    for i, v := range data {
        j := i
        for ;j > 0 && v < data[j - 1]; j-- {
            data[j] = data[j - 1]
        }
        data[j] = v
    }
}
