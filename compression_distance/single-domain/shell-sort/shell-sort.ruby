module ShellSort
  def self.sort(keys)
    sort!(keys.clone)
  end
  
  def self.sort!(keys)
    gap = keys.size/2
    while gap > 0
      for j in gap...keys.size
        key = keys[j]
        i = j
        while (i >= gap and keys[i-gap] > key)
          keys[i] = keys[i-gap]
          i -= gap
        end
        keys[i] = key
      end
      gap /= 2
    end
    keys
  end
end

