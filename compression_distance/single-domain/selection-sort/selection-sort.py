newList = [6,7,4,5,1]
for outer in range(len(newList)):
    minimum = min(newList[outer:]) #find minimum element
    minIndex = newList[outer:].index(minimum) #find index of minimum element
    newList[outer + minIndex] = newList[outer] #replace element at minIndex with first element
    newList[outer] = minimum #replace first element with min element
print newList

