sub swap {
    @_[ 0, 1 ] = @_[ 1, 0 ];
}

sub bubble_sort {
    # returns a sorted copy 
    my (@a) = @_;
    for my $i ( 0 .. $#a ) {
        for my $j ( 0 .. $#a - 1 - $i) {
            swap $a[$j], $a[ $j + 1 ]
              if $a[$j] > $a[ $j + 1 ];
        }
    }
    \@a;
}
