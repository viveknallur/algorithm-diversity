def qsort(l: List[Int]): List[Int] = {
    l match {
        case List() => l
        case _ =>  qsort(for(x <- l.tail if x < l.head) yield x) ::: List(l.head) ::: qsort(for(x <-1.tail if x >= l.head) yield x)
    }
}
