func QSort(data []int) {
    if len(data) < 2 {
        return
    }
    pivot := data[0]
    l, r := 1, len(data) - 1
    for l <= r {
        for l <= r && data[l] <= pivot {
            l ++
        }
        for r >= l && data[r] >= pivot {
            r --
        }
        if l < r {
            data[l], data[r] = data[r], data[l]
        }
    }
 
    if r > 0 {
        data[0], data[r] = data[r], data[0]
        qsort(data[0:r])
    }
    qsort(data[l:])
}
