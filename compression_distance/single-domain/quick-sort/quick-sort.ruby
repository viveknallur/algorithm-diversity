def quicksort(list)
   return list if list.length <= 1
   pivot = list.shuffle.shift
   left, right = list.partition { |el| el < pivot }
   quicksort(left).concat(quicksort(right))
end
