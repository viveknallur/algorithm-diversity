# README #
This repository contains all of the code from experiments on algorithmic-diversity. By algorithmic-diversity, we mean a pool of algorithms that generate different, yet correct, output to the same input sequence.

### What is this repository for? ###
There are two tracks of experiments, that we're currently conducting:

* Measuring the impact of diversifying algorithms on a system: What happens when the system under test swaps algorithms every 'k' timeperiods? What happens when multiple agents inside the system, start using different algorithms to control their actions? These questions are sought to be answered by the load-balancing and the minority-game experiments, respectively.

* Measuring the distance between a pool of algorithms: Assuming that we have a pool of algorithms, how diverse are they? Can we tell in an automated manner, regardless of the programming language used? This experiment uses differences in compression lengths, as a mechanism to quantify the differences between any two implementations of a program.


### Who do I talk to? ###

* If you have ideas for experiments, on the same or different domains, or would like to talk about algorithmic diversity, get in touch with Vivek Nallur (the maintainer of this repository).